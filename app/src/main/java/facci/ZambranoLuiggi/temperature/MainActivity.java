package facci.ZambranoLuiggi.temperature;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btnConvertir;
    TextView resultado;
    EditText entrada;
    Double inputNumber;
    RadioButton celsius, fahrenheit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("Aplicacion Movil 1", "Zambrano Franco Luiggi Andres");

        entrada = (EditText) findViewById(R.id.entrada);
        btnConvertir = (Button) findViewById(R.id.btnConvertir);
        resultado = (TextView) findViewById(R.id.resultado);
        celsius = (RadioButton) findViewById(R.id.celsius);
        fahrenheit = (RadioButton) findViewById(R.id.fahrenheit);

        btnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputText = entrada.getText().toString();

                if (inputText.isEmpty()) {
                    resultado.setText("Ingresar la cantidad.");
                } else {
                    inputNumber = Double.parseDouble(inputText);

                    if (inputNumber <= 0) {
                        resultado.setText("Ingrese un numero mayor a 0.");
                    } else {
                        if (celsius.isChecked() || fahrenheit.isChecked()) {
                            if (celsius.isChecked()) {
                                Double r = (inputNumber - 32) * 5/9 ;
                                resultado.setText(String.valueOf(r) + " °C");
                            } else {
                                Double r = (inputNumber * 9) /5 + 32 ;
                                resultado.setText(String.valueOf(r) + " °F");
                            }
                        } else {
                            resultado.setText("Seleccione uno de los datos");
                        }
                    }
                }
            }
        });
    }
}